package com.petani.tescitralanggengsentosa.support_custom;

public interface OnBackPressedListener {
    void onBackPressed();
}
