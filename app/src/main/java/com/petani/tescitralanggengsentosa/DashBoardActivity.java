package com.petani.tescitralanggengsentosa;

import android.app.ActivityOptions;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.petani.tescitralanggengsentosa.menu0.MainActivity;
import com.petani.tescitralanggengsentosa.menu1.CRUDActivity;
import com.petani.tescitralanggengsentosa.support_custom.OnBackPressedListener;

import java.text.DecimalFormat;
import java.util.List;

public class DashBoardActivity extends AppCompatActivity {
private Button btnMenu0, btnMenu1, btnMenu2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        toolsView();
    }

    private void toolsView() {
        btnMenu0 = (Button) findViewById(R.id.btnMenu0);
        btnMenu1 = (Button) findViewById(R.id.btnMenu1);
        btnMenu2 = (Button) findViewById(R.id.btnMenu2);

        final Bundle bundle = ActivityOptions.makeCustomAnimation(getApplicationContext(),
                android.R.anim.fade_in , android.R.anim.cycle_interpolator).toBundle();

        btnMenu0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DashBoardActivity.this, MainActivity.class);
                startActivity(intent,bundle);

                finish();
            }
        });

        btnMenu1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(DashBoardActivity.this, CRUDActivity.class);
//                startActivity(intent,bundle);
                startActivity(CRUDActivity.getActIntent(DashBoardActivity.this));

                finish();
            }
        });

        btnMenu2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(DashBoardActivity.this, R.string.note_onprogress, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        /*super.onBackPressed();*/
        boolean isCanShowAlertDialog = false;
        List<Fragment> fragmentList = getSupportFragmentManager().getFragments();
        if (fragmentList != null) {
            //TODO: Perform your logic to pass back press here
            for (Fragment fragment : fragmentList) {
                if (fragment instanceof OnBackPressedListener) {
                    isCanShowAlertDialog = true;
                    ((OnBackPressedListener) fragment).onBackPressed();
                }
            }
        }
        if (!isCanShowAlertDialog) {
            showExitDialogConfirmation();
        }
    }

    void showExitDialogConfirmation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.confirm_exit)
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
        Button positive = alert.getButton(AlertDialog.BUTTON_POSITIVE);
        positive.setTextColor(Color.BLACK);
        Button negative = alert.getButton(AlertDialog.BUTTON_NEGATIVE);
        negative.setTextColor(Color.BLACK);
    }

    public static String numberFormat(int source){
        if(source == -1) return "";
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        String yourFormattedString = formatter.format(source);
        return yourFormattedString.replaceAll(",",".");
    }
}
