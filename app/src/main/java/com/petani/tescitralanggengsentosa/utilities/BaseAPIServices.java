package com.petani.tescitralanggengsentosa.utilities;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface BaseAPIServices {
//    @FormUrlEncoded
    @POST("wcs/robot/job/submit")
    public Call<ResponseBody> postQuicktron(@Header("Authorization") String token,
                                            @Body RequestBody payload);

}
