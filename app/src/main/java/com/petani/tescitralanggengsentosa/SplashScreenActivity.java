package com.petani.tescitralanggengsentosa;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

public class SplashScreenActivity extends Activity {
    private Context context = SplashScreenActivity.this;
    private static long SPLASH_DELAY = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        context = this;

        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                showHome();
            }
        };
        Timer timer = new Timer();
        timer.schedule(task, SPLASH_DELAY);

    }

    private void showHome() {

        Bundle bundle = ActivityOptions.makeCustomAnimation(getApplicationContext(),
                android.R.anim.fade_in , android.R.anim.slide_out_right).toBundle();

        Intent intent = new Intent(SplashScreenActivity.this, DashBoardActivity.class);
        startActivity(intent, bundle);

        finish();
    }
}
