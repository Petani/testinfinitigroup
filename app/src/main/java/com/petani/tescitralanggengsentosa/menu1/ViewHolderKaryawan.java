package com.petani.tescitralanggengsentosa.menu1;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.petani.tescitralanggengsentosa.R;
import com.petani.tescitralanggengsentosa.models.Karyawan;

import java.text.DecimalFormat;

public class ViewHolderKaryawan extends RecyclerView.ViewHolder {
    public Karyawan listKaryawan;
    public LinearLayout layoutKaryawanCard;
    private TextView textKaryawan, textDivisi, textGaji;
    private Button btnWarna;

    public ViewHolderKaryawan(View itemView) {
        super(itemView);

        this.layoutKaryawanCard = (LinearLayout) itemView.findViewById(R.id.layoutListKaryawan);
        this.textKaryawan = (TextView) itemView.findViewById(R.id.textKaryawan);
        this.textDivisi   = (TextView) itemView.findViewById(R.id.textDivisi);
        this.textGaji     = (TextView) itemView.findViewById(R.id.textGaji);
        this.btnWarna     = (Button) itemView.findViewById(R.id.btnWarna);
    }

    public void setListModel(Karyawan listModel){
        this.listKaryawan = listModel;

        this.textKaryawan.setText(listModel.getNama_karyawan());
        this.textDivisi.setText(listModel.getDivisi_karyawan());
        this.textGaji.setText("Rp. "+numberFormat(Integer.parseInt(listModel.getGaji_karyawan())));

        if (Integer.parseInt(listModel.getGaji_karyawan()) >= 10000001) {
            this.btnWarna.setBackgroundColor(Color.RED);
            this.btnWarna.setText("Mahal");
            this.btnWarna.setTextSize(8);
        }
        if (Integer.parseInt(listModel.getGaji_karyawan()) <= 10000000) {
            this.btnWarna.setBackgroundColor(Color.GREEN);
            this.btnWarna.setText("Murah");
            this.btnWarna.setTextSize(8);
        }
    }

    public static String numberFormat(int source){
        if(source == -1) return "";
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        String yourFormattedString = formatter.format(source);
        return yourFormattedString.replaceAll(",",".");
    }
}
