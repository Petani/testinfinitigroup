package com.petani.tescitralanggengsentosa.menu0;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.petani.tescitralanggengsentosa.DashBoardActivity;
import com.petani.tescitralanggengsentosa.R;
import com.petani.tescitralanggengsentosa.models.SendPost;
import com.petani.tescitralanggengsentosa.support_custom.DialogJobID;
import com.petani.tescitralanggengsentosa.support_custom.OnBackPressedListener;
import com.petani.tescitralanggengsentosa.support_custom.ProgressLoading;
import com.petani.tescitralanggengsentosa.utilities.APIUtilities;
import com.petani.tescitralanggengsentosa.utilities.BaseAPIServices;

import java.util.List;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private APIUtilities apiUtilities;
    BaseAPIServices apiServices;

    private final Context mContext = MainActivity.this;

    private EditText inputPost, inputRobotJobID, inputBucketID, inputCoordinateX, inputCoordinateY,
            inputWorkFace, inputWarehouseID, inputLetDownFlag, inputJobPriority, inputURL;

    private String valuePost,valueRobotJobId, valueWarehouseId,valueBucketID, valueWorkFace,
                    valueEndPoint, valueLetDownFlag, valueJobPriority;
    private String json;

    private Button btnSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolsView();
    }

    private void toolsView() {
        inputPost = findViewById(R.id.inputPost);
        inputRobotJobID = findViewById(R.id.inputRobotJobID);
        inputBucketID = findViewById(R.id.inputBucketID);
        inputCoordinateX = findViewById(R.id.inputCoordinateX);
        inputCoordinateY = findViewById(R.id.inputCoordinateY);
        inputWorkFace = findViewById(R.id.inputWorkFace);
        inputWarehouseID = findViewById(R.id.inputWarehouseID);
        inputLetDownFlag = findViewById(R.id.inputLetDownFlag);
        inputJobPriority = findViewById(R.id.inputJobPriority);
        inputURL = findViewById(R.id.inputURL);

        btnSend = findViewById(R.id.btnSend);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickedSend();
            }
        });

        inputPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getSupportFragmentManager();
                DialogJobID editNameDialogFragment = DialogJobID.newInstance("Some Title");
                editNameDialogFragment.show(fm, "fragment_edit_name");

            }
        });
    }

    private void onClickedSend() {
        valuePost        = inputPost.getText().toString().trim();
        valueRobotJobId  = inputRobotJobID.getText().toString().trim();
        valueWarehouseId = inputWarehouseID.getText().toString().trim();
        valueBucketID    = inputBucketID.getText().toString().trim();
        valueWorkFace    = inputWorkFace.getText().toString().trim();
        valueEndPoint    = inputURL.getText().toString().trim();
        valueLetDownFlag = inputLetDownFlag.getText().toString().trim();
        valueJobPriority = inputJobPriority.getText().toString().trim();

        boolean isEmptyText = false;

        if(TextUtils.isEmpty(valuePost)){
            isEmptyText = true;
            inputPost.setError(getString(R.string.note_fill_blank));
        }
        if(TextUtils.isEmpty(valueRobotJobId)){
            isEmptyText = true;
            inputRobotJobID.setError(getString(R.string.note_fill_blank));
        }
        if(TextUtils.isEmpty(valueWarehouseId)){
            isEmptyText = true;
            inputWarehouseID.setError(getString(R.string.note_fill_blank));
        }
        if(TextUtils.isEmpty(valueBucketID)){
            isEmptyText = true;
            inputBucketID.setError(getString(R.string.note_fill_blank));
        }
        if(TextUtils.isEmpty(valueWorkFace)){
            isEmptyText = true;
            inputWorkFace.setError(getString(R.string.note_fill_blank));
        }
        if(TextUtils.isEmpty(valueLetDownFlag)){
            isEmptyText = true;
            inputLetDownFlag.setError(getString(R.string.note_fill_blank));
        }
        if(TextUtils.isEmpty(valueJobPriority)){
            isEmptyText = true;
            inputJobPriority.setError(getString(R.string.note_fill_blank));
        }
        if (!isEmptyText){
            String json = processDataForSendAsJsonString(valuePost,valueRobotJobId,valueWarehouseId,valueBucketID,
                    valueWorkFace,valueEndPoint,valueLetDownFlag,valueJobPriority);
            RequestBody body = RequestBody.create(APIUtilities.mediaType(), json);
            Log.d(this.getClass().toString(), "=== RequestBody Create : " + json);

            submitPost();
        }

    }

    public String processDataForSendAsJsonString(String valuePost, String valueRobotJobId, String valueWarehouseId, String valueBucketID,
                                                 String valueWorkFace, String valueEndPoint,String valueLetDownFlag, String valueJobPriority) {
        SendPost data = new SendPost(valueRobotJobId,valueWarehouseId,valueBucketID,valueWorkFace,valueEndPoint,valueLetDownFlag,valueJobPriority);

        data.setRobotJobId(inputRobotJobID.getText().toString());
        data.setWarehouseId(inputWarehouseID.getText().toString());
        data.setBucketId(inputBucketID.getText().toString());
        data.setWorkFace(inputWorkFace.getText().toString());
        data.setEndPoint(inputURL.getText().toString());
        data.setLetDownFlag(inputLetDownFlag.getText().toString());
        data.setJobPriority(inputJobPriority.getText().toString());

        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.serializeNulls().create();
        String json = gson.toJson(data);
        return json;
    }

    private void submitPost() {
    final ProgressDialog progressDialog = ProgressLoading.generateNonCancelableLoadingSimple(mContext);
    final String authorization = "author";

    final String json = processDataForSendAsJsonString(valuePost, valueRobotJobId,valueWarehouseId,valueBucketID,
            valueWorkFace,valueEndPoint,valueLetDownFlag,valueJobPriority);

    RequestBody body = RequestBody.create(APIUtilities.mediaType(), json);
    progressDialog.show();

    apiServices = APIUtilities.getAPIServiceMain();
    apiServices.postQuicktron(authorization,body).enqueue(new Callback<ResponseBody>() {
        @Override
        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
            try {
                if (response.isSuccessful() && response.code() == 200){
                    if (progressDialog.isShowing()){
                        progressDialog.dismiss();
                    }
                    Toast.makeText(mContext, "Status Post isSuccessful", Toast.LENGTH_SHORT).show();
                    moveDashBoard();
                }else {
                    if (progressDialog.isShowing()){
                        progressDialog.dismiss();
                    }
                    Toast.makeText(mContext, "Status POST is NotSuccessful", Toast.LENGTH_SHORT).show();
                    errorMove();
                }

            }catch (Exception error){
                error.printStackTrace();
                Log.e(this.getClass().toString(), "=== onResponse function submitException : " + error.getMessage(), error);
                moveDashBoard();
            }
        }

        @Override
        public void onFailure(Call<ResponseBody> call, Throwable t) {
            if (progressDialog.isShowing()){
                progressDialog.dismiss();
            }
            if (mContext != null) Toast.makeText(mContext, "Status onFailure" + " JSON POST : " + json, Toast.LENGTH_LONG).show();
            errorMove();
        }
    });
    }

    private void errorMove() {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Error, Apakah Anda ingin kembali ke halaman sebelumnya ?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            moveDashBoard();
                            finish();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
            Button positive = alert.getButton(AlertDialog.BUTTON_POSITIVE);
            positive.setTextColor(Color.BLACK);
            Button negative = alert.getButton(AlertDialog.BUTTON_NEGATIVE);
            negative.setTextColor(Color.BLACK);
    }

    private void moveDashBoard() {
        Bundle bundle = ActivityOptions.makeCustomAnimation(getApplicationContext(),
                android.R.anim.fade_in , android.R.anim.slide_out_right).toBundle();

        Intent intent = new Intent(MainActivity.this, DashBoardActivity.class);
        startActivity(intent, bundle);

        finish();
    }

    @Override
    public void onBackPressed() {
        /*super.onBackPressed();*/
        boolean isCanShowAlertDialog = false;
        List<Fragment> fragmentList = getSupportFragmentManager().getFragments();
        if (fragmentList != null) {
            //TODO: Perform your logic to pass back press here
            for (Fragment fragment : fragmentList) {
                if (fragment instanceof OnBackPressedListener) {
                    isCanShowAlertDialog = true;
                    ((OnBackPressedListener) fragment).onBackPressed();
                }
            }
        }
        if (!isCanShowAlertDialog) {
            showExitDialogConfirmation();
        }
    }

    void showExitDialogConfirmation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.confirm_cancel_post)
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        moveDashBoard();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
        Button positive = alert.getButton(AlertDialog.BUTTON_POSITIVE);
        positive.setTextColor(Color.BLACK);
        Button negative = alert.getButton(AlertDialog.BUTTON_NEGATIVE);
        negative.setTextColor(Color.BLACK);
    }

}
