package com.petani.tescitralanggengsentosa.utilities;

import okhttp3.MediaType;

public class APIUtilities {
    public static MediaType mediaType() {
        return okhttp3.MediaType.parse("application/json; charset=utf-8");
    }
    public static final class ServiceEndpoint {
        public static final String MAIN     = "http://192.168.20.20:8080/";
        /*Token OAUTH yang dibutuhkan untuk lokal */
//        public static final String OAUTH = "https://192.168.20.20:8080/token";
    }

    public static BaseAPIServices getAPIServiceMain(){
        return RetrofitClient.getClient(ServiceEndpoint.MAIN).create(BaseAPIServices.class);
    }
}
