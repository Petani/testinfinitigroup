package com.petani.tescitralanggengsentosa.menu1;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.petani.tescitralanggengsentosa.R;
import com.petani.tescitralanggengsentosa.models.Karyawan;

import java.util.ArrayList;
import java.util.List;

public class AdapterKaryawanRecyclerView extends RecyclerView.Adapter<ViewHolderKaryawan> {

    private ArrayList<Karyawan> models;
    private Context mContext;
    FirebaseDataListener listener;

    public AdapterKaryawanRecyclerView(ArrayList<Karyawan> karyawanArrayList, Context context){
        /**
         * Inisiasi data dan variabel yang akan digunakan
         */
        models = karyawanArrayList;
        mContext = context;
        listener = (CRUDActivity)context;
    }



    @NonNull
    @Override
    public ViewHolderKaryawan onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_karyawan, viewGroup, false);
        return new ViewHolderKaryawan(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderKaryawan viewHolderKaryawan, final int i) {
        final Karyawan model = models.get(i);
        viewHolderKaryawan.setListModel(model);

        viewHolderKaryawan.layoutKaryawanCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext, "Ready For Detail Data Karyawan", Toast.LENGTH_LONG).show();

            }
        });

        viewHolderKaryawan.layoutKaryawanCard.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Toast.makeText(mContext, "Ready For Edit Data Karyawan", Toast.LENGTH_LONG).show();

                final Dialog dialog = new Dialog(mContext);
                dialog.setContentView(R.layout.layout_dialog_crud);
                dialog.setTitle("Pilih Aksi");
                dialog.show();

                Button editButton = (Button) dialog.findViewById(R.id.bt_edit_data);
                Button delButton = (Button) dialog.findViewById(R.id.bt_delete_data);

                //apabila tombol edit diklik
                editButton.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                                mContext.startActivity(CRUDActivity.getActIntent((Activity) mContext).putExtra("data", models.get(i)));
                            }
                        }
                );

                //apabila tombol delete diklik
                delButton.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                                listener.onDeleteData(models.get(i), i);
                            }
                        }
                );
                return true;
            }
        });

    }

    public interface FirebaseDataListener{
        void onDeleteData(Karyawan karyawan, int position);
    }

    @Override
    public int getItemCount() {
        if (models != null){
            return models.size();
        }
        return 0;
    }
}
