package com.petani.tescitralanggengsentosa.support_custom;

import android.app.ProgressDialog;
import android.content.Context;

import com.petani.tescitralanggengsentosa.R;


public class ProgressLoading {
    public static ProgressDialog generateNonCancelableLoadingSimple(Context context){
        ProgressDialog progressDialog = new ProgressDialog(context, R.style.SimpleProgressDialogTheme);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setProgressStyle(android.R.style.Widget_ProgressBar_Small);

        return progressDialog;
    }
}
