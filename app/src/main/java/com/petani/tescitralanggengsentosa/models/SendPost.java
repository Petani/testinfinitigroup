package com.petani.tescitralanggengsentosa.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SendPost {
    @SerializedName("robotJobId")
    @Expose
    private String robotJobId;

    @SerializedName("warehouseId")
    @Expose
    private String warehouseId;

    @SerializedName("bucketId")
    @Expose
    private String bucketId;

    @SerializedName("workFace")
    @Expose
    private String workFace;

    @SerializedName("endPoint")
    @Expose
    private String endPoint;

    @SerializedName("letDownFlag")
    @Expose
    private String letDownFlag;

    @SerializedName("jobPriority")
    @Expose
    private String jobPriority;

    public String getRobotJobId() {
        return robotJobId;
    }

    public void setRobotJobId(String robotJobId) {
        this.robotJobId = robotJobId;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getBucketId() {
        return bucketId;
    }

    public void setBucketId(String bucketId) {
        this.bucketId = bucketId;
    }

    public String getWorkFace() {
        return workFace;
    }

    public void setWorkFace(String workFace) {
        this.workFace = workFace;
    }

    public String getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(String endPoint) {
        this.endPoint = endPoint;
    }

    public String getLetDownFlag() {
        return letDownFlag;
    }

    public void setLetDownFlag(String letDownFlag) {
        this.letDownFlag = letDownFlag;
    }

    public String getJobPriority() {
        return jobPriority;
    }

    public void setJobPriority(String jobPriority) {
        this.jobPriority = jobPriority;
    }

    public SendPost(String robotJobId, String warehouseId, String bucketId, String workFace, String endPoint, String letDownFlag, String jobPriority) {
        this.robotJobId = robotJobId;
        this.warehouseId = warehouseId;
        this.bucketId = bucketId;
        this.workFace = workFace;
        this.endPoint = endPoint;
        this.letDownFlag = letDownFlag;
        this.jobPriority = jobPriority;
    }
}
