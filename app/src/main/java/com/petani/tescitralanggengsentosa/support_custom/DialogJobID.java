package com.petani.tescitralanggengsentosa.support_custom;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.petani.tescitralanggengsentosa.R;

public class DialogJobID extends DialogFragment {
    private EditText mEditText, inputFilter;
    private TextView titleDialog;
    private ListView listData;
    private ImageView closeBtnDialog;


    public DialogJobID() {
        // Constructor kosong diperlukan untuk DialogFragment.
        // Pastikan tidak memberikan argument/parameter apapun ke
        // constructor ini.
    }

    public static DialogJobID newInstance(String title) {
        DialogJobID frag = new DialogJobID();
        Bundle args = new Bundle();
        args.putString("title", title);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.layout_dialog, container, false);

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getDialog().setCanceledOnTouchOutside(false);

        ImageView declineButton = (ImageView) rootView.findViewById(R.id.closeBtnDialog);
        declineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Close dialog
                getDialog().dismiss();
            }
        });

        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Baca view yang dibuat di XML
//        mEditText = (EditText) view.findViewById(R.id.txt_your_name);
        titleDialog = (TextView) view.findViewById(R.id.titleDialog);
        inputFilter = (EditText) view.findViewById(R.id.inputFilter);
        listData    = (ListView) view.findViewById(R.id.listData);
        closeBtnDialog = view.findViewById(R.id.closeBtnDialog);


        // Ambil argument dari bundle (yang ada di newInstance) lalu mengatur title dari Dialog
        // yang ditampilkan dengan data di dalam bundle
        String title = getArguments().getString("title", "Enter Name");
        getDialog().setTitle(title);
        // Show soft keyboard automatically and request focus to field
//        mEditText.requestFocus();
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }
}
