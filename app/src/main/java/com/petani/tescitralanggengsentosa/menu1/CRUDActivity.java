package com.petani.tescitralanggengsentosa.menu1;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.petani.tescitralanggengsentosa.DashBoardActivity;
import com.petani.tescitralanggengsentosa.R;
import com.petani.tescitralanggengsentosa.models.Karyawan;
import com.petani.tescitralanggengsentosa.support_custom.OnBackPressedListener;

import java.util.ArrayList;
import java.util.List;

public class CRUDActivity extends AppCompatActivity implements AdapterKaryawanRecyclerView.FirebaseDataListener{
    private Button btnDaftar;
    private EditText inputNama,inputDivisi, inputGaji, inputPassword;
    private String valueNama,valueDivisi,valueGaji,valuePassword;

    private RecyclerView listKaryawan;

    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    Integer passwordAdmin = 123456;

    private RecyclerView recyclerView;
    private List<Karyawan> karyawanList = new ArrayList<>();
    private AdapterKaryawanRecyclerView adapterKaryawan;

    Context mContext;

    private LinearLayout layoutDaftar, layoutList;

    private DatabaseReference database;
    private ArrayList<Karyawan> daftarKaryawan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crud);
        FirebaseApp.initializeApp(this);

        btnDaftar     = (Button)   findViewById(R.id.btnDaftar);
        inputNama     = (EditText) findViewById(R.id.inputNama);
        inputGaji     = (EditText) findViewById(R.id.inputGaji);
        inputDivisi   = (EditText) findViewById(R.id.inputDivisi);
        inputPassword = (EditText) findViewById(R.id.inputPassword);

        layoutDaftar  = (LinearLayout) findViewById(R.id.layoutDaftar);
        layoutList    = (LinearLayout) findViewById(R.id.layoutList);

        listKaryawan  = (RecyclerView) findViewById(R.id.listKaryawan);
        listKaryawan.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        listKaryawan.setLayoutManager(layoutManager);

        database = FirebaseDatabase.getInstance().getReference();
        database.child("karyawan").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                /**
                 * Saat ada data baru, masukkan datanya ke ArrayList
                 */
                daftarKaryawan = new ArrayList<>();
                for (DataSnapshot noteDataSnapshot : dataSnapshot.getChildren()) {
                    Karyawan karyawan= noteDataSnapshot.getValue(Karyawan.class);
                    karyawan.setKey(noteDataSnapshot.getKey());
                    daftarKaryawan.add(karyawan);
                }
                adapter = new AdapterKaryawanRecyclerView(daftarKaryawan, CRUDActivity.this);
                listKaryawan.setAdapter(adapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(CRUDActivity.this, "JANGAN NONGOL", Toast.LENGTH_SHORT).show();
            }
        });

        final Karyawan karyawan= (Karyawan) getIntent().getSerializableExtra("data");

        if (karyawan != null) {
            inputNama.setText(karyawan.getNama_karyawan());
            inputDivisi.setText(karyawan.getDivisi_karyawan());
            inputGaji.setText(karyawan.getGaji_karyawan());
            btnDaftar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    karyawan.setNama_karyawan(inputNama.getText().toString());
                    karyawan.setDivisi_karyawan(inputDivisi.getText().toString());
                    karyawan.setGaji_karyawan(inputGaji.getText().toString());
                    inputPassword.setText("");

                    updateKaryawan(karyawan);
                }
            });
        } else {
            btnDaftar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    validation();
                }
            });
        }
    }

    private void validation() {
        valueNama     = inputNama.getText().toString();
        valueGaji     = inputGaji.getText().toString();
        valueDivisi   = inputDivisi.getText().toString();
        valuePassword = inputPassword.getText().toString();
        Log.d("cekit",valueNama+","+valueDivisi+","+valueGaji+","+valuePassword);

        boolean isEmptyText = false;

        if(TextUtils.isEmpty(valueNama)){
            isEmptyText = true;
            inputNama.setError(getString(R.string.note_fill_blank));
        }
        if(TextUtils.isEmpty(valueDivisi)){
            isEmptyText = true;
            inputDivisi.setError(getString(R.string.note_fill_blank));
        }
        if(TextUtils.isEmpty(valueGaji)){
            isEmptyText = true;
            inputGaji.setError(getString(R.string.note_fill_blank));
        }
        if (TextUtils.isEmpty(valuePassword)){
            isEmptyText = true;
            inputPassword.setError(getString(R.string.note_fill_blank));
        }
        if (!isEmptyText){
            daftarKaryawan();
        }
    }

    private void daftarKaryawan() {
        if (Integer.parseInt(inputPassword.getText().toString()) != passwordAdmin){
            inputPassword.setError("Passwordnya 123456");
        }else{
            if(!isEmpty(inputNama.getText().toString()) && !isEmpty(inputDivisi.getText().toString()) && !isEmpty(inputGaji.getText().toString())) {
                submitKaryawan(new Karyawan(inputNama.getText().toString(), inputDivisi.getText().toString(), inputGaji.getText().toString()));
                Toast.makeText(this, "Berhasil Daftar, All Filled !!!", Toast.LENGTH_LONG).show();
            }else
                Snackbar.make(findViewById(R.id.btnDaftar), "Please Fill in the blank field !", Snackbar.LENGTH_LONG).show();

            InputMethodManager imm = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(
                    inputNama.getWindowToken(), 0);
        }
    }

    private void submitKaryawan(Karyawan karyawan) {
        database.child("karyawan").push().setValue(karyawan).addOnSuccessListener(this, new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                inputNama.setText("");
                inputDivisi.setText("");
                inputGaji.setText("");
                Snackbar.make(findViewById(R.id.btnDaftar), "Employee successfully registered.", Snackbar.LENGTH_LONG).show();
            }
        });
    }

    private void updateKaryawan(Karyawan karyawan) {
        database.child("karyawan") //akses parent index, ibaratnya seperti nama tabel
                .child(karyawan.getKey()) //select barang berdasarkan key
                .setValue(karyawan) //set value barang yang baru
                .addOnSuccessListener(this, new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Snackbar.make(findViewById(R.id.btnDaftar), "Employee successfully updated. !", Snackbar.LENGTH_LONG).setAction("See", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                inputPassword.setText("");
                                finish();
                            }
                        }).show();
                    }
                });
    }

    private void moveDashBoard() {
        Bundle bundle = ActivityOptions.makeCustomAnimation(getApplicationContext(),
                android.R.anim.fade_in , android.R.anim.slide_out_right).toBundle();

        Intent intent = new Intent(CRUDActivity.this, DashBoardActivity.class);
        startActivity(intent, bundle);

        finish();
    }

    @Override
    public void onBackPressed() {
        /*super.onBackPressed();*/
        boolean isCanShowAlertDialog = false;
        List<Fragment> fragmentList = getSupportFragmentManager().getFragments();
        if (fragmentList != null) {
            //TODO: Perform your logic to pass back press here
            for (Fragment fragment : fragmentList) {
                if (fragment instanceof OnBackPressedListener) {
                    isCanShowAlertDialog = true;
                    ((OnBackPressedListener) fragment).onBackPressed();
                }
            }
        }
        if (!isCanShowAlertDialog) {
            showExitDialogConfirmation();
        }
    }

    void showExitDialogConfirmation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.back_to_dashboard)
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        moveDashBoard();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
        Button positive = alert.getButton(AlertDialog.BUTTON_POSITIVE);
        positive.setTextColor(Color.BLACK);
        Button negative = alert.getButton(AlertDialog.BUTTON_NEGATIVE);
        negative.setTextColor(Color.BLACK);
    }

    public static Intent getActIntent(Activity activity) {
        return new Intent(activity, CRUDActivity.class);
    }

    @Override
    public void onDeleteData(Karyawan karyawan, final int position) {
        if(database!=null){
            database.child("karyawan").child(karyawan.getKey()).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Toast.makeText(CRUDActivity.this,"Employee successfully deleted.", Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    private boolean isEmpty(String s){
        return TextUtils.isEmpty(s);
    }
}
